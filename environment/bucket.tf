resource "random_string" "bucket_suffix" {
  length           = 10
  special          = false
  number           = true
  lower            = true
  upper            = false
}

resource "google_storage_bucket" "training_backend" {
  name          = "stack-labs-terraform-training-backend-${random_string.bucket_suffix.result}" 
  location      = upper(var.google_region) 
  force_destroy = true 
}

resource "local_file" "backend_bucket_name" {
    content     = google_storage_bucket.training_backend.name 
    filename = "${path.module}/bucket-name.txt"
}