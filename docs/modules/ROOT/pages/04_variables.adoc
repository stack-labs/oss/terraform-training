:experimental:
:checkpoint-sha: 53867ce7703fc11541383f331a49195a341e7662

= Variables

**Covered features**: link:https://www.terraform.io/docs/language/values/variables.html[input variables], link:https://www.terraform.io/docs/language/values/locals.html[local values], link:https://www.terraform.io/docs/language/expressions/strings.html#interpolation[string interpolation]

include:::partial$checkpoint.adoc[]

== What is a _variable_ ?

Variables serve as parameters for `terraform`, allowing aspects of a deployment to be customized without altering the source code.

The link:https://www.terraform.io/docs/language/values/index.html[official documentation] defines several types of variables.

=== Input variables 

Input variables are meant to customize behavior at _apply time_.

[source,hcl]
----
variable "tag" { # <1>
  type = string # <2>
  default = "latest" # <3>
}
----
<1> The `variable` keyword followed by a name defines an input variable
<2> The `type` parameter defines (obviously) the type of the variable
<3> The `default` parameter specifies a default value in case you did not provide one

=== Local values

Those are a convenience feature for assigning a short name to an expression.

[source,hcl]
----
locals { # <1>
  image = "nginx" # <2>
}
----
<1> The `locals` block is meant to hold as many local variables as you need
<2> A name and a value for the variable

They are used as follows:

[source,hcl]
----
resource "type" "name" {
  ...
  parameter = "prefix-${local.image}" # <1> <2>
  ...
}
----
<1> To refer to a local value, use the `local.` prefix and append the name of the local variable
<2> Here is an example of link:https://www.terraform.io/docs/language/expressions/strings.html#interpolation[string interpolation] used to concatenate two strings. The syntax is similar to link:https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals[Javascript ES6 interpolation]


== Defining an input variable

._You have been keeping my credentials out in the open?! Fix that at once or I'll strip you down to the bones!_
[quote, Colonel Hashicorp D.S]
image:colonel-scared.png[]


As noticed by the colonel, our deployment of `minio` requires two environment variables to define username and password of the root user. However, it is *not good practice* to have the values of those variables hardcoded in your `terraform` code. Let's change that to pass them at _apply time_!

✏ Create a file called `variables.tf` next to your `main.tf` with the following content:

[.copy]
[source,hcl]
----
variable "minio_user" {
    type = string
}
variable "minio_password" {
    type = string
}
----

✏ Reference those variables in the `env` configuration of your `minio` container called `minio_tf` as such:

[.copy]
[source,hcl]
----
env = [ 
  "MINIO_ROOT_USER=${var.minio_user}", # <1>
  "MINIO_ROOT_PASSWORD=${var.minio_password}",
]
----
<1> The `var.` prefix allows you to reference any variable defined in your configuration

✏ Give it a shot and try applying your configuration: 

[.copy]
[source,bash]
----
Λ\: $ terraform apply -auto-approve
----

`terraform` should ask your for your input as such:

[#terraform-prompts]
[source,bash]
----
var.minio_password
  Enter a value: 
----

And then:

[source,bash]
----
var.minio_user
  Enter a value:
----

✏ Go ahead and fill those values, and hit kbd:[Enter] each time. Use the following values: 

(Password)
[.copy]
[source,bash]
----
david-sanders
----

(User)
[.copy]
[source,txt]
----
colonel-hashicorp
----

(Output)

[source,bash]
----
docker_container.minio_tf: Refreshing state... [id=28a550911e5fa006959aaf6558a1b531eb87d13cffc392c252dc66cc7738f2af]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following
symbols:
-/+ destroy and then create replacement

Terraform will perform the following actions:

  # docker_container.minio_tf must be replaced
-/+ resource "docker_container" "minio_tf" {
      + bridge            = (known after apply)
      + container_logs    = (known after apply)
      - cpu_shares        = 0 -> null
      - dns               = [] -> null
      - dns_opts          = [] -> null
      - dns_search        = [] -> null
      ~ entrypoint        = [
          - "/usr/bin/docker-entrypoint.sh",
        ] -> (known after apply)
      ~ env               = [ # forces replacement
          - "MINIO_ROOT_PASSWORD=david-sanders", # <1>
          + "MINIO_ROOT_USER=colonel-hashicorp", # <2>
            # (1 unchanged element hidden)
        ]
      + exit_code         = (known after apply)
      ~ gateway           = "172.17.0.1" -> (known after apply)
      - group_add         = [] -> null
      ~ hostname          = "28a550911e5f" -> (known after apply)
      ~ id                = "28a550911e5fa006959aaf6558a1b531eb87d13cffc392c252dc66cc7738f2af" -> (known after apply)
      ~ init              = false -> (known after apply)
      ~ ip_address        = "172.17.0.2" -> (known after apply)
      ~ ip_prefix_length  = 16 -> (known after apply)
      ~ ipc_mode          = "private" -> (known after apply)
      - links             = [] -> null
      - log_opts          = {} -> null
      - max_retry_count   = 0 -> null
      - memory            = 0 -> null
      - memory_swap       = 0 -> null
        name              = "minio-container"
      ~ network_data      = [
          - {
              - gateway                   = "172.17.0.1"
              - global_ipv6_address       = ""
              - global_ipv6_prefix_length = 0
              - ip_address                = "172.17.0.2"
              - ip_prefix_length          = 16
              - ipv6_gateway              = ""
              - network_name              = "bridge"
            },
        ] -> (known after apply)
      - network_mode      = "default" -> null
      - privileged        = false -> null
      - publish_all_ports = false -> null
      ~ security_opts     = [] -> (known after apply)
      ~ shm_size          = 64 -> (known after apply)
      - storage_opts      = {} -> null
      - sysctls           = {} -> null
      - tmpfs             = {} -> null
        # (13 unchanged attributes hidden)

      + healthcheck {
          + interval     = (known after apply)
          + retries      = (known after apply)
          + start_period = (known after apply)
          + test         = (known after apply)
          + timeout      = (known after apply)
        }

      + labels {
          + label = (known after apply)
          + value = (known after apply)
        }


      - volumes {
          - container_path = "/data" -> null
          - host_path      = "/absolute/path/to/supplies" -> null
          - read_only      = false -> null
        }
      + volumes {
          + container_path = "/data"
          + host_path      = "/absolute/path/to/supplies"
        }
        # (1 unchanged block hidden)
    }

Plan: 1 to add, 0 to change, 1 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes 

docker_container.minio_tf: Destroying... [id=28a550911e5fa006959aaf6558a1b531eb87d13cffc392c252dc66cc7738f2af]
docker_container.minio_tf: Destruction complete after 1s
docker_container.minio_tf: Creating...
docker_container.minio_tf: Creation complete after 1s [id=655cd3ffbd81d5c7e4a3a92ebba40bd441423240852318f008697d89a4c5e5ef]

Apply complete! Resources: 1 added, 0 changed, 1 destroyed.
----
<1> Notice the variables being resolved in the plan

This is looking good, however, we are still able to see the credentials in the logs. Luckily, with the usage of the `sensitive` parameter, we can fix that.

✏ Modify the `variables.tf` file to add:

[.copy]
[source,hcl]
----
sensitive = true
----

To both the variables you just created as follows:

[source,hcl]
----
variable "minio_user" {
    type = string
    sensitive = true
}
variable "minio_password" {
    type = string
    sensitive = true
}
----

✏ Apply the configuration and take a peek at the logs

[.copy]
[source,bash]
----
Λ\: $ terraform apply -auto-approve
----

👀 You should see something close to the following lines in the output:

[source,bash]
----
...

# Warning: this attribute value will be marked as sensitive and will not
# display in UI output after applying this change.
 ~ env               = (sensitive) # forces replacement <1>

...
----
<1> You can see that the sensitive values have been hidden

== Providing values

Defining variables is cool, but it is useless unless you can fill them with values. Variables must be filled with values at runtime, meaning when your run `terraform plan` or `terraform apply`.

There are several ways to do so depending on how and where you mean to use `terraform`.

=== Interactive mode

This is the most straightforward way of providing values to `terraform`. You simply let `terraform` prompt you for input within the terminal when required.

NOTE: You have *already used this method* in the xref:#terraform-prompts[previous section]

This method is rarely used since it requires a lots of interactions between `terraform` and the user and does not scale properly. However, when building a new `terraform` deployment, it can prove to be useful for temporary testing.

=== Through the command line arguments

Another way of providing variable values in the terminal is by using command line arguments. The `-var` flag is what you are looking for.

✏ Go ahead and try providing the variable values using the `-var` flag:

[.copy]
[source,bash]
----
Λ\: $ terraform apply -var minio_user=colonel-hashicorp -var minio_password=david-sanders
----

(Output)

[source,bash]
----
No changes. Your infrastructure matches the configuration.

Your configuration already matches the changes detected above. If you'd like to update the Terraform state to match, create and apply a
refresh-only plan:
  terraform apply -refresh-only

Apply complete! Resources: 0 added, 0 changed, 0 destroyed.
----

TIP: Since you *did not change* the values but simply *the way you provide them*, `terraform` does not see any changes.

👀 As you can see, `terraform` did not prompt you for input

This is useful when you need to provide variables but do not have access to environnement variables and file system, or want to use directly the output of another command. You hit the limits of this method when you need to define complex variables such as objects or lists.

=== *With a `.tfvars` file*

Another way of providing variables is through a `.tfvar` file. Those files are to be written in the HCL `terraform` language and prove to be very useful when you need to pass complex objects. Let's give it a shot, shall we ?

✏ Create a file called `input.tfvars` next to you `variables.tf` file with the following content:

[.copy]
[source,hcl]
----
minio_user = "colonel-hashicorp"
minio_password = "david-sanders"
----

✏ Apply the new configuration using given the file: 
[.copy]
[source,bash]
----
Λ\: $ terraform apply -var-file="input.tfvars"
----

(Output)

[source,bash]
----
docker_container.minio_tf: Refreshing state... [id=5b4df29c2ff96923cb01e23ff7fa3179aa2200042c55f01a3441d84e19aecebf]

No changes. Your infrastructure matches the configuration.

Terraform has compared your real infrastructure against your configuration and found no differences, so no changes are needed.

Apply complete! Resources: 0 added, 0 changed, 0 destroyed.
----

NOTE: As already state in the previous section, since you provided the same values for the variables, there were no changes to apply

This method is good when you have numerous values to pass or complex ones. However, it requires you to have access to the file system to provide the file, and may leave artifacts with sensitive data if you do not take care properly of those files.

TIP: If you name your `.tfvars` file *`terraform.tfvars` or* `**.auto.tfvars`*, it will be *picked up automatically* when you run `terraform apply` without having to specify the `-var-file` flag.

TIP: You can also *provide variables files as `.json`* files

// === Through environment variables

The last method of providing variables is by defining environment variables. `terraform` searches the environment of its own process for environment variables named `TF_VAR_` followed by the name of a declared variable.

WARNING: On *operating systems* where environment *variable names are case-sensitive*, `terraform` *matches the variable name exactly* as given in configuration, and so the required environment variable name will usually have a mix of upper and lower case letters

✏ Define environment variables to hold the values of the variables you need to provide: 

[.copy]
[source,bash]
----
Λ\: $ export TF_VAR_minio_user=colonel-hashicorp

Λ\: $ export TF_VAR_minio_password=david-sanders
----

✏ Go ahead and try applying the configuration: 

[.copy]
[source,bash]
----
Λ\: $ terraform apply
----

(Output)

[source,bash]
----
docker_container.minio_tf: Refreshing state... [id=0e3145b1f9666fc656de83e873454ed5406c38a5367fe7bfd40452dc4ffba99e]

No changes. Your infrastructure matches the configuration.

Terraform has compared your real infrastructure against your configuration and found no differences, so no changes are needed.

Apply complete! Resources: 0 added, 0 changed, 0 destroyed.
----

This method defacto the one used in CI/CD but does not scale if you need to define a lot of values.
