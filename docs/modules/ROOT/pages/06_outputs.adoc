:experimental:
:checkpoint-sha: 83ce87135665e397265826392c65759706aec52e

= Outputs

**Covered features**: link:https://www.terraform.io/docs/language/values/outputs.html[output values]

include:::partial$checkpoint.adoc[]

== What is an _output value_ ?

Output values are like the return values of a `terraform` configuration, and are generally used to print certain values in the CLI output after running terraform apply.

Declaring an output value is very straightforward: 

[source,hcl]
----
output "nginx_ip_address" { # <1>
  value = docker_container.nginx_tf.ip_address # <2>
}
----
<1> Use the `output` keyword followed by the name you want to give your output
<2> The `value` parameter contains the value you want to exposed, in this case the IP address of the `nginx_tf` terraform resource

Once defined, retrieving the value is as easy as running: 

[source,bash]
----
Λ\: $ terraform output nginx_ip_address # <1>
----
<1> `nginix_ip_address` being the name of the output you just defined

== Defining an output

._Where the hell are my credentials!? Give them back to me or I'll start throwing tenders at you!_
[quote, Colonel Hashicorp D.S]
image:colonel-angry.jpg[]


In the previous section, you used the random provider to generate a username and a password for your `minio` root credentials. However, should you need to login with those credentials, you are *missing a way of retrieving them*. Let's *define outputs to do just that*!

✏ Create a file called `outputs.tf` with the following content: 

[.copy]
[source,hcl]
----
output "root_user" {
    value = random_pet.minio_user.id
}

output "root_password" {
    value = random_password.minio_password.result
}
----

✏ Try applying the configuration and see what `terraform` tells you: 

[.copy]
[source,bash]
----
Λ\: $ terraform apply
----

(Output)

[source,bash]
----
random_pet.minio_user: Refreshing state... [id=radically-real-basilisk]
random_password.minio_password: Refreshing state... [id=none]
docker_container.minio_tf: Refreshing state... [id=92d42545843c91239e31b3b7cdd02e8767664a80cee0632ca6f8847f5a36a267]
╷
│ Error: Output refers to sensitive values
│ 
│   on outputs.tf line 5:
│    5: output "root_password" {
│ 
│ To reduce the risk of accidentally exporting sensitive data that was intended to be only internal, Terraform requires that any root
│ module output containing sensitive data be explicitly marked as sensitive, to confirm your intent.
│ 
│ If you do intend to export this data, annotate the output value as sensitive by adding the following argument:
│     sensitive = true
╵
----

CAUTION: When defining outputs, it is *important to know exactly what they are meant for* to *avoid creating an involuntary leak* of sensitive data 

✏ Since `terraform` complains, follow its recommandations to fix the error by adding:  

[.copy]
[source,hcl]
----
sensitive = true
----

To your password output as follows:

[source,hcl]
----
output "root_password" {
    value = random_password.minio_password.result
    sensitive = true
}
----

✏ You can now apply the configuration again: 

[.copy]
[source,bash]
----
Λ\: $ terraform apply -auto-approve
---- 

(Output)

[source,bash]
----
random_pet.minio_user: Refreshing state... [id=radically-real-basilisk]
random_password.minio_password: Refreshing state... [id=none]
docker_container.minio_tf: Refreshing state... [id=92d42545843c91239e31b3b7cdd02e8767664a80cee0632ca6f8847f5a36a267]

Changes to Outputs: # <1>
  + root_password = (sensitive value) # <2> 
  + root_user     = "radically-real-basilisk" # <3>

You can apply this plan to save these new output values to the Terraform state, without changing any real infrastructure.

Apply complete! Resources: 0 added, 0 changed, 0 destroyed. # <4>

Outputs:

root_password = <sensitive> # <2>
root_user = "radically-real-basilisk" # <3>
----
<1> `terraform` detected you changed the outputs
<2> The `root_password` being sensitive, it is not displayed in the logs 
<3> The `root_user` is now visible, since it is not considered sensitive by the random provider, and you did not add the parameter `sensitive = true`
<4> No changes to the infrastructure meaning there is nothing to do in terms of resources

== Retrieving the value of an output

This is good but we still do not have access to the password. Because it is sensitive data, it requires an explicit action to retrieve its content.

✏ Use the `terraform` CLI to output the password:

[[_output-root-password]]
[.copy]
[source,bash]
----
Λ\: $ terraform output root_password
----

(Output)

[source,bash]
----
"bCrxA9QwnCfNve6k" # <1>
----
<1> The leading and trailing `"` characters are not part of the value

IMPORTANT: By default, `terraform` outputs are in *human readable* format, which is *close the JSON format*.

TIP: To see the *raw output, use the `-raw` flag* just after the `output` command

✏ Visit link:http://localhost:9001[] and verify the credentials are working properly

[TIP]
====
If you need to retrieve the *username* again without *looking at the logs*, simply run:
[.copy]
[source,bash]
----
Λ\: $ terraform output root_user
----
====

