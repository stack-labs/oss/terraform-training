:experimental:
:checkpoint-sha: 403f2fac6c18203b8ca3a3bf40f00f45eaf2d6f6

= Workspaces

**Covered features**: link:https://www.terraform.io/docs/language/state/workspaces.html[workspaces], link:https://www.terraform.io/docs/language/state/workspaces.html#current-workspace-interpolation[workspace interpolation], link:https://www.terraform.io/docs/language/state/index.html[state], link:https://www.terraform.io/docs/language/state/locking.html[state locking], link:https://github.com/hashicorp/terraform/issues/15339#issuecomment-322542209[backup state]

include:::partial$checkpoint.adoc[]

== What is a _workspace_ ?

In `terraform`, workspaces are separate instances of state data that can be used from the same working directory. You can use workspaces to manage multiple non-overlapping groups of resources with the same configuration.

Managing workspace is as easy as running: 

[source,bash]
----
Λ\: $ terraform workspace # <1>

Usage: terraform [global options] workspace

  new, list, show, select and delete Terraform workspaces
----
<1> Add the action you want to perform.

As mentioned in the xref:01_basics#_workspaces-cli[previous chapters], this command is meant to create, delete and switch between multiple targeted environments when deploying the same infrastructure code.

NOTE: Up *until now*, you *have been using the `default` workspace*. This is default behavior of `terraform` meaning usage of workspaces is not mandatory if you do not need them.

The state for each workspace is stored in a `terraform.tfstate.d/*` folder, where `&#42;`is the name of the workspace.

TIP: Since you have been using *the `default` workspace*, everything you did was *tracked in the `terraform.tfstate` file* at the root of your working directory

CAUTION: You *can not delete the `default` workspace*, so make sure not to remove the `terraform.tfstate` file

== What is a _state_ ?

=== The state

As mentioned above, everything you apply is tracked in the state. `terraform` uses state data to remember which real-world object corresponds to each resource in the configuration; this allows it to modify an existing object when its resource declaration changes.

This state is stored by default in a local file named `terraform.tfstate`. `terraform` uses this local state to create plans and make changes to your infrastructure. Prior to any operation, it does a refresh to update the state with the real infrastructure.

The primary purpose of the state is to store bindings between objects in a remote system and resource instances declared in your configuration. When `terraform` creates a remote object in response to a change of configuration, it will record the identity of that remote object against a particular resource instance, and then potentially update or delete that object in response to future configuration changes.

👀 You can safely open the `terraform.tfstate` file and inspect it's content

As you can see, it is a JSON file containing all the information about your current infrastructure.

IMPORTANT: The `&#42;.tfstate` file contains *sensitive data*. If you inspected it thoroughly, you saw that the *sensitive outputs are stored in plain text*. It is essential to *manage the lifecycle of those files carefully*! 

=== The lockfile

When applying a configuration, you may have noticed a `.terraform.tfstate.lock.info` file being created and deleted next to your `terraform.tfstate` file. It is called the *lockfile* and is meant to avoid race ; `terraform` locks your state for all operations that could write state. This prevents other processes from acquiring the lock and potentially corrupting your state.

State locking happens automatically on all operations that could write state. You won't see any message that it is happening. If state locking fails, `terraform` will not continue. You can disable state locking for most commands with the `-lock` flag but it is not recommended.

=== The backup state

The last file in your working directory you may be wondering about is the `terraform.tfstate.backup` file. This backup file is created locally to allow for it to be used to recover in the event of an erroneous update.

👀 Go ahead an open it, you should see similar information as in the `terraform.tfstate` file, though corresponding to the configuration be your last `apply` command

== Using workspaces

._The first restaurant is a success buddy! I am so happy we are going to expand with another one!_
[quote, Colonel Hashicorp D.S]
image:colonel-raise-hands.jpg[]

It's time to start using workspaces! Before creating a new one, we will make a few changes to your configuration to avoid having conflicts during deployment.

=== Making your infrastructure _workspace-friendly_

NOTE: Since we are *deploying locally* using the `docker` API, we need to *make sure the containers won't use the same resources* on your machine resulting in conflicts.

✏ Add the following lines to your `minio.tf` file: 

[.copy]
[source,hcl]
----
resource "random_integer" "minio_port" {
  min = 9000
  max = 9999
  seed = "${terraform.workspace}" # <1> <2> 
}
----
<1> This ensures predictable results for the port number depending on the workspace name
<2> Using `terraform.workspace`, we are able to access the name of the workspace in the resources

TIP: We are using the *random provider* to *create a workspace dependant port number* to be used by the `minio` container. That way, every workspace will require a different port of the host machine minimizing the risk of conflicting containers.

✏ In the same file as above, modify the `minio_tf` resource of type `docker_container` to use this port as follows:

[.copy]
[source,hcl]
----
ports {
  internal = 9001
  external = random_integer.minio_port.result
}
----

✏ Finally, change the name of `docker_container` resource `minio_tf` so that it does not conflict between workspace and can be easy identified: 

[.copy]
[source,hcl]
----
name  = "minio-container-${terraform.workspace}" # <1>
----
<1> As shown above, we are using string interpolation to make the name of the container workspace dependant

✏ Let's apply the configuration and see how it goes: 

[.copy]
[source,bash]
----
Λ\: $ terraform apply
----

(Output)

[source,bash]
----
random_password.minio_password: Refreshing state... [id=none]
random_pet.minio_user: Refreshing state... [id=sadly-generous-crawdad]
docker_container.minio_tf: Refreshing state... [id=f4ea0cc0df737a23e75dce0d411211e0a0c339dbddf13b7df9ed6dee5990289f]
Note: Objects have changed outside of Terraform

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following
symbols:
  + create
-/+ destroy and then create replacement

Terraform will perform the following actions:

  # docker_container.minio_tf must be replaced
-/+ resource "docker_container" "minio_tf" {
      ~ bridge            = "" -> (known after apply)
      + container_logs    = (known after apply)
      - cpu_set           = "" -> null
      - cpu_shares        = 0 -> null
      - dns               = [] -> null
      - dns_opts          = [] -> null
      - dns_search        = [] -> null
      - domainname        = "" -> null
      ~ entrypoint        = [
          - "/usr/bin/docker-entrypoint.sh",
        ] -> (known after apply)
      + exit_code         = (known after apply)
      ~ gateway           = "172.17.0.1" -> (known after apply)
      - group_add         = [] -> null
      ~ hostname          = "f4ea0cc0df73" -> (known after apply)
      ~ id                = "f4ea0cc0df737a23e75dce0d411211e0a0c339dbddf13b7df9ed6dee5990289f" -> (known after apply)
      ~ init              = false -> (known after apply)
      ~ ip_address        = "172.17.0.2" -> (known after apply)
      ~ ip_prefix_length  = 16 -> (known after apply)
      ~ ipc_mode          = "private" -> (known after apply)
      - links             = [] -> null
      - log_opts          = {} -> null
      - max_retry_count   = 0 -> null
      - memory            = 0 -> null
      - memory_swap       = 0 -> null
      ~ name              = "minio-container" -> "minio-container-default" # forces replacement
      ~ network_data      = [
          - {
              - gateway                   = "172.17.0.1"
              - global_ipv6_address       = ""
              - global_ipv6_prefix_length = 0
              - ip_address                = "172.17.0.2"
              - ip_prefix_length          = 16
              - ipv6_gateway              = ""
              - network_name              = "bridge"
            },
        ] -> (known after apply)
      - network_mode      = "default" -> null
      - pid_mode          = "" -> null
      - privileged        = false -> null
      - publish_all_ports = false -> null
      ~ security_opts     = [] -> (known after apply)
      ~ shm_size          = 64 -> (known after apply)
      - storage_opts      = {} -> null
      - sysctls           = {} -> null
      - tmpfs             = {} -> null
      - user              = "" -> null
      - userns_mode       = "" -> null
      - working_dir       = "" -> null
        # (14 unchanged attributes hidden)

      + healthcheck {
          + interval     = (known after apply)
          + retries      = (known after apply)
          + start_period = (known after apply)
          + test         = (known after apply)
          + timeout      = (known after apply)
        }

      + labels {
          + label = (known after apply)
          + value = (known after apply)
        }

      ~ ports {
          ~ external = 9001 -> (known after apply) # forces replacement
            # (3 unchanged attributes hidden)
        }

      - volumes {
          - container_path = "/data" -> null
          - host_path      = "/absolute/path/to/supplies" -> null
          - read_only      = false -> null
        }
      + volumes {
          + container_path = "/data"
          + host_path      = "/absolute/path/to/supplies"
        }
    }

  # random_integer.minio_port will be created
  + resource "random_integer" "minio_port" {
      + id     = (known after apply)
      + max    = 9999
      + min    = 9000
      + result = (known after apply)
      + seed   = "default"
    }

Plan: 2 to add, 0 to change, 1 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value:
----

✏ Review the proposed changes, type `yes` and press kbd:[Enter] to confirm

✏ Inspect the newly created container: 

[.copy]
[source,bash]
----
Λ\: $ docker ps
----

(Output)

[.copy]
[source,bash]
----
CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS          PORTS                              NAMES
77f6919bfda2   1afc478341fd   "/usr/bin/docker-ent…"   41 seconds ago   Up 40 seconds   9000/tcp, 0.0.0.0:9570->9001/tcp   minio-container-default
----

NOTE: Your *port* number *may differ*

=== Using workspaces

Let's create a new workspace as the colonel requested!

✏ Create a new workspace called `expand`: 

[.copy]
[source,bash]
----
Λ\: $ terraform workspace new expand
----

(Output)

[source,bash]
----
Created and switched to workspace "expand"!

You're now on a new, empty workspace. Workspaces isolate their state,
so if you run "terraform plan" Terraform will not see any existing state
for this configuration. 
----

👀 Take a look at your `terraform.tfstate.d` directory and notice how a new subfolder called `expand` has been created

✏ Go ahead an apply the configuration: 

[.copy]
[source,bash]
----
Λ\: $ terraform apply
----

(Output)

[source,bash]
----
Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following
symbols:
  + create

Terraform will perform the following actions:

  # docker_container.minio_tf will be created
  + resource "docker_container" "minio_tf" {
      + attach           = false
      + bridge           = (known after apply)
      + command          = [
          + "server",
          + "/data",
          + "--console-address",
          + ":9001",
        ]
      + container_logs   = (known after apply)
      + entrypoint       = (known after apply)
      + env              = (sensitive)
      + exit_code        = (known after apply)
      + gateway          = (known after apply)
      + hostname         = (known after apply)
      + id               = (known after apply)
      + image            = "sha256:1afc478341fd5f3ca3c85e41ab97a04f81ec7560ee614ce5b85398f5cea811ad"
      + init             = (known after apply)
      + ip_address       = (known after apply)
      + ip_prefix_length = (known after apply)
      + ipc_mode         = (known after apply)
      + log_driver       = "json-file"
      + logs             = false
      + must_run         = true
      + name             = "minio-container-expand"
      + network_data     = (known after apply)
      + read_only        = false
      + remove_volumes   = true
      + restart          = "no"
      + rm               = false
      + security_opts    = (known after apply)
      + shm_size         = (known after apply)
      + start            = true
      + stdin_open       = false
      + tty              = false

      + healthcheck {
          + interval     = (known after apply)
          + retries      = (known after apply)
          + start_period = (known after apply)
          + test         = (known after apply)
          + timeout      = (known after apply)
        }

      + labels {
          + label = (known after apply)
          + value = (known after apply)
        }

      + ports {
          + external = (known after apply)
          + internal = 9001
          + ip       = "0.0.0.0"
          + protocol = "tcp"
        }

      + volumes {
          + container_path = "/data"
          + host_path      = "/home/agent-k/Documents/stack-labs/terraform-training/.ignore/ws/supplies"
        }
    }

  # random_integer.minio_port will be created
  + resource "random_integer" "minio_port" {
      + id     = (known after apply)
      + max    = 9999
      + min    = 9000
      + result = (known after apply)
      + seed   = "expand"
    }

  # random_password.minio_password will be created
  + resource "random_password" "minio_password" {
      + id               = (known after apply)
      + length           = 16
      + lower            = true
      + min_lower        = 0
      + min_numeric      = 0
      + min_special      = 0
      + min_upper        = 0
      + number           = true
      + override_special = "_-@{}*"
      + result           = (sensitive value)
      + special          = true
      + upper            = true
    }

  # random_pet.minio_user will be created
  + resource "random_pet" "minio_user" {
      + id        = (known after apply)
      + length    = 3
      + separator = "-"
    }

Plan: 4 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + root_password = (sensitive value)
  + root_user     = (known after apply)

Do you want to perform these actions in workspace "expand"?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: 
----

As expected, nothing exists in this workspace so `terraform` is starting fresh

✏ Review the plan, type `yes` and press kbd:[Enter] to apply the configuration

(Output)

[source,bash]
----
random_pet.minio_user: Creating...
random_integer.minio_port: Creating...
random_password.minio_password: Creating...
random_pet.minio_user: Creation complete after 0s [id=immensely-grown-beagle]
random_integer.minio_port: Creation complete after 0s [id=9175]
random_password.minio_password: Creation complete after 0s [id=none]
docker_container.minio_tf: Creating...
docker_container.minio_tf: Creation complete after 1s [id=fdd6a3a86d2da80f6a5e4c93b0d7cfb2cf5060119064da64c20e89b478a5ce7e]

Apply complete! Resources: 4 added, 0 changed, 0 destroyed.

Outputs:

root_password = <sensitive>
root_user = "immensely-grown-beagle" # <1>
----
<1> Your `root_user` output may differ

✏ Inspect the result in terms of running containers: 

[.copy]
[source,bash]
----
Λ\: $ docker ps
----

(Output) 

[source,bash]
----
CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS          PORTS                              NAMES
fdd6a3a86d2d   1afc478341fd   "/usr/bin/docker-ent…"   34 seconds ago   Up 33 seconds   9000/tcp, 0.0.0.0:9175->9001/tcp   minio-container-expand
77f6919bfda2   1afc478341fd   "/usr/bin/docker-ent…"   10 minutes ago   Up 10 minutes   9000/tcp, 0.0.0.0:9570->9001/tcp   minio-container-default
----

NOTE: Your *port* numbers *may differ*

🎊 Congratulations, you have just deployed the exact same configuration in the same environment using workspaces.

✏ Take a look a the Web UI using the correct port for each container, and check that both have access to the `tenders` bucket on both of them

NOTE: *Both containers* run using the *`./supplies` folder mounted as a volume*. Inside, as you can recall, we created the `tenders` bucket, which can be accessed using either one of the containers. This shows you that *correctly configuring your infrastructure may help* you make *full usage of your existing resources*. It is definitely interesting to leverage when you can not duplicate shared resources such as databases or firewalls.

[TIP]
====
If you want to *connect* to either of the exposed Web UIs, you need to *retrieve the credentials* using the `output` command:

[.copy]
[source,bash]
----
Λ\: $ terraform workspace select default # <1>
----
<1> or `expand` depending on the one you want inspect

And then as shown in the xref:06_outputs.adoc#_output-root-password[previous chapter]: 

[.copy]
[source,bash]
----
Λ\: $ terraform output -raw root_password # <1>
----
<1> Or `root_user` should you need it
====
